<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;

class DataService {

    const INSTR_QUERY = 'QUERY';
    const INSTR_UPDATE = 'UPDATE';
    const FOLDER_RESULTS = 'public';
    const FOLDER_STORAGE = 'storage';
    
    protected $cubeService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CubeService $cubeService) {
        $this->cubeService = $cubeService;
    }

    public function handleRiddle($puzzle) {
        $file_handle = fopen(storage_path("app/" . $puzzle), "r");
        $results = "";
        $line_of_text = fgets($file_handle);
        $tests = intval($line_of_text);
        for ($z = 0; $z < $tests; $z++) {
            $line_of_text = fgets($file_handle);
            $parts = explode(' ', $line_of_text);
            if (count($parts) == 2) {
                $size = intval($parts[0]);
                $tests = intval($parts[1]);
                if ($size > 0 && $tests > 0) {
                    $this->cubeService->createCube($size);
                    $results = $this->runInstructions($tests,$file_handle,$results);
                }
            }
        }
        fclose($file_handle);
        $path = self::FOLDER_RESULTS . "/" . date('Y-m-d_h-m-s') . ".txt";
        Storage::put($path, $results, 'public');
        return self::FOLDER_STORAGE . "/" . date('Y-m-d_h-m-s') . ".txt";;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function runInstructions($tests, $file_handle,$results) {
        for ($y = 0; $y < $tests; $y++) {
            $line_of_text = fgets($file_handle);
            $partsQuery = explode(' ', $line_of_text);
            if (count($partsQuery) == 5 || count($partsQuery) == 7) {
                if ($partsQuery[0] == self::INSTR_QUERY && count($partsQuery) == 7) {
                    $total = $this->cubeService->queryData(
                            intval($partsQuery[1]), intval($partsQuery[2]), intval($partsQuery[3]), intval($partsQuery[4]), intval($partsQuery[5]), intval($partsQuery[6])
                    );
                    $results .= $total . "\n";
                } else if ($partsQuery[0] == self::INSTR_UPDATE && count($partsQuery) == 5) {
                    $this->cubeService->updateData(
                            intval($partsQuery[1]), intval($partsQuery[2]), intval($partsQuery[3]), floatval($partsQuery[4])
                    );
                }
            }
        }
        return $results;
    }

}
