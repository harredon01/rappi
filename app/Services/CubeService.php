<?php

namespace App\Services;

use App\Cube;
use Illuminate\Http\Response;
use DB;

class CubeService {

    const CUBE_TABLE = 'cubes';
    const CUBE_LENGTH = 'length';
    const CUBE_WIDTH = 'width';
    const CUBE_HEIGHT = 'height';
    const CUBE_VALUE = 'value';

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function createCube($size) {
        DB::table('cubes')->truncate();
        for ($x = 0; $x < $size; $x++) {
            $inserts = [];
            for ($y = 0; $y < $size; $y++) {
                for ($z = 0; $z < $size; $z++) {
                    $data = [
                        self::CUBE_LENGTH => $x + 1,
                        self::CUBE_WIDTH => $y + 1,
                        self::CUBE_HEIGHT => $z + 1,
                        self::CUBE_VALUE => 0,
                    ];
                    array_push($inserts, $data);
                }
            }
            Cube::insert($inserts);
            $inserts = [];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function updateData($length, $width, $height, $value) {
        $cube = Cube::where(self::CUBE_LENGTH, $length)
                ->where(self::CUBE_WIDTH, $width)
                ->where(self::CUBE_HEIGHT, $height)
                ->first();
        $cube->value = $value;
        $cube->save();
        $data = [
            "status" => "error",
            "message" => "file does not exist",
        ];
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function queryData($x1, $y1, $z1, $x2, $y2, $z2) {
        $cube = DB::table(self::CUBE_TABLE)
                ->where(self::CUBE_LENGTH, ">=", $x1)
                ->where(self::CUBE_LENGTH, "<=", $x2)
                ->where(self::CUBE_WIDTH, ">=", $y1)
                ->where(self::CUBE_WIDTH, "<=", $y2)
                ->where(self::CUBE_HEIGHT, ">=", $z1)
                ->where(self::CUBE_HEIGHT, "<=", $z2)
                ->sum(self::CUBE_VALUE);
        return $cube;
    }
}
