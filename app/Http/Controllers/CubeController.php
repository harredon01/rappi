<?php

namespace App\Http\Controllers;

use App\Cube;
use App\Jobs\SolvePuzzle;
use App\Services\DataService;
use Illuminate\Http\Request;

class CubeController extends Controller {

    protected $dataService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DataService $dataService) {
        $this->dataService = $dataService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRiddle(Request $request) {
        $path = "";
        if ($request->hasFile('puzzle')) {
            $path = $request->file('puzzle')->store('puzzles');
            //dispatch(new SolvePuzzle($path));
            $path = $this->dataService->handleRiddle($path);
        }
        return view('register')->with("path", $path);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cube  $cube
     * @return \Illuminate\Http\Response
     */
    public function show(Cube $cube) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cube  $cube
     * @return \Illuminate\Http\Response
     */
    public function edit(Cube $cube) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cube  $cube
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cube $cube) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cube  $cube
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cube $cube) {
        //
    }

}
