<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RefactoringController extends Controller {

    const STATUS_DENIED = '6';
    const ERROR_DENIED = '2';
    const ERROR_INVALID_DRIVER = '3';
    const ERROR_INVALID_SERVICE = '4';
    const ERROR_SERVICE_UNAVAILABLE = '5';
    const ERROR_NO_ERROR = '0';
    const STATUS_ACTIVE = '1';
    const STATUS_PROCESSING = '2';
    const DRIVER_STATUS_BUSY = '0';
    const SERVICE_ACTIVE = '2';
    const USER_IPHONE = '1';
    const USER_ANDROID = '2';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function post_confirm() {
        $id = Input::get('service_id');
        $servicio = Service::find($id);
        //dd($servicio);
        if ($servicio != NULL) {
            if ($servicio->status_id == "6") {
                return Response::json(array('error' => '2'));
            }
            if ($servicio->driver_id == NULL && $servicio->status_id == "1") {
                $servicio = Service::update($id, array(
                            'driver_id' => Input::get('driver_id'),
                            'status_id' => '2'
                                //Up Carro
                                //,'pwd' => md5(Input::get('pwd'))
                ));
                Driver::update(Input::get('driver_id'), array(
                    "available" => '0'
                ));
                $driverTmp = Driver::find(Input::get('driver_id'));
                Service::update($id, array(
                    'car_id' => Input::get('car_id'),
                        //Up Carro
                        //,'pwd' => md5(Input::get('pwd'))
                ));
                //Notificar a usuario!!
                $pushMessage = 'Tu servicio ha sido confirmado!';
                /* $servicio = Service::find($id);
                 * $push = Push::make();
                 * if($servicio->user->type == '1'){//iPhone
                 * $pushAns = $push->ios($servicio->user->uuid, $pushMessage);
                 * } else {
                 * $pushAns = $push->android($servicio->user->uuid, $pushMessage);
                 * }
                 */
                $servicio = Service::find($id);
                $push = Push::make();
                if ($servicio->user->uuid == '') {
                    return Response::json(array('error' => '0'));
                }
                if ($servicio->user->type == '1') {//IPHONE
                    $result = $push->ios($servicio->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
                } else {
                    $result = $push->android2($servicio->user->uuid, $pushMessage, 1, 'default', 'Open', array('serviceId' => $servicio->id));
                }
                return Response::json(array('error' => '0'));
            } else {
                return Response::json(array('error' => '1'));
            }
        } else {
            return Response::json(array('error' => '1'));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notifyUserConfirmation(Service $service) {
        $pushMessage = 'Tu servicio ha sido confirmado!';
        $push = Push::make();
        $user_uuid = $service->user->uuid;
        $user_type = $service->user->type;
        if ($user_uuid && $user_type) {
            if ($user_type == self::USER_IPHONE) {
                $push->ios($user_uuid, $pushMessage, 1, 'honk.wav', 'Open', array('service_id' => $service->id));
            } else if ($user_type == self::USER_ANDROID) {
                $push->android($user_uuid, $pushMessage, 1, 'default', 'Open', array('service_id' => $service->id));
            }
        }
    }

    public function assignServiceToDriver() {
        $id = Input::get('service_id');
        $service = Service::find($id);
        if ($service) {
            if ($service->status_id == self::STATUS_DENIED) {
                $message = [
                    "status" => "error",
                    "error" => self::ERROR_DENIED,
                    "message" => "Service is unavailable"
                ];
                return Response::json($message);
            }
            if ($service->driver_id == NULL && $service->status_id == self::STATUS_ACTIVE) {
                $driver = Driver::find(Input::get('driver_id'));
                if ($driver) {
                    $service->driver_id = $driver->id;
                    $service->status_id = self::STATUS_PROCESSING;
                    $service->car_id = $driver->car_id;
                    $driver->status_id = self::DRIVER_STATUS_BUSY;
                    $driver->save();
                    $service->save();

                    $this->notifyUserConfirmation($service);
                    $message = [
                        "status" => "success",
                        "error" => self::ERROR_NO_ERROR,
                        "message" => "Service Assigned to driver"
                    ];
                    return Response::json($message);
                } else {
                    $message = [
                        "status" => "error",
                        "error" => self::ERROR_INVALID_DRIVER,
                        "message" => "Driver not found"
                    ];
                    return Response::json($message);
                }
            } else {
                $message = [
                    "status" => "error",
                    "error" => self::ERROR_SERVICE_UNAVAILABLE,
                    "message" => "Service is already active"
                ];
                return Response::json($message);
            }
        } else {
            $message = [
                "status" => "error",
                "error" => self::ERROR_INVALID_SERVICE,
                "message" => "Service not found"
            ];
            return Response::json($message);
        }
    }

}
