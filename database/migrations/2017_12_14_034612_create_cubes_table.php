<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCubesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cubes', function (Blueprint $table) {
		$table->increments('id');
		$table->double('width', 11, 2);
		$table->double('length', 11, 2);
		$table->double('height', 11, 2);
		$table->double('value', 11, 2);
		$table->index('width');
		$table->index('length');
		$table->index('height');
		$table->index('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cubes');
    }
}
