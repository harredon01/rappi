<?php

namespace Tests\Unit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CubeTest extends TestCase
{
    protected $cubeService;


    public function setUp() {
        parent::setUp();
        $this->cubeService = $this->app->make('App\Services\CubeService');
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->cubeService->createCube(4);
        $this->cubeService->updateData(2, 2, 2, 4);
        $this->assertTrue($this->cubeService->queryData(1, 1, 1, 3, 3, 3)==4);
        $this->cubeService->updateData(1, 1, 1, 23);
        $this->assertTrue($this->cubeService->queryData(2, 2, 2, 4, 4, 4)==4);
        $this->assertTrue($this->cubeService->queryData(1, 1, 1, 3, 3, 3)==27);
        
        $this->cubeService->createCube(2);
        $this->cubeService->updateData(2, 2, 2, 1);
        $this->assertTrue($this->cubeService->queryData(1, 1, 1, 1, 1, 1)==0);
        $this->assertTrue($this->cubeService->queryData(1, 1, 1, 2, 2, 2)==1);
        $this->assertTrue($this->cubeService->queryData(2, 2, 2, 2, 2, 2)==1);
    }
}
